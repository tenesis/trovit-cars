Trovit
===================

El principal objetivo de este repositorio es la de diseñar una arquitectura que permita a Trovit gestionar la gran cantidad de anuncios de coches, de manera que puedan ser almacenados y consultados en tiempo real con alta disponibilidad. 

Uno de los principales requisitos de este proyecto es la capacidad de escalado horizontal de la arquitectura ya que debe ser capaz de asumir un flujo de consultas de anuncios y procesado de los mismos. 

----------

> **Note:**

> - El despliegue de toda la arquitectura se realizar mediante docker.


DataWarehouse
-------------

La forma en la que se van a almacenar los datos será sobre un datawarehouse construido con mongo, la elección de esta base de datos es por la capacidad de escalado y el buen rendimiento que da sobre la lectura de documentos indexados.

> **Note:**

> - Para el primer diseño del datawarehouse la BBDD será un mongo, pero dependiendo de la naturaleza de los datos o los requisitos de negocio este podría cambiar.

Ya que los anuncios tienen una naturaleza temporal y pueden expirar habría que realizar un proceso de expiración de los anuncios.
Si se quisiera almacenar los anuncios de una forma más persistentes, se podría insertar los datos sobre una base de datos distribuido y escalable como HDFS.


----------


Feed
-------------------

La alimentación del proyecto se realiza en forma de streaming con un sistema de mensajería de colas (Kafka) para ello se despliega 1 broker kafka con un zookeeper ( topic: cars_feed ), en caso de escalado, kafka ofrece la posibilidad de aumentar los recursos tanto de almacenamiento, particiones, replicación y máquinas para conseguir emitir todos los anuncios nuevos que el sistema de búsqueda es capaz de encontrar.


----------


Procesado de datos feed
-------------

El procesado de datos en streaming se hace con kafka como fuente de datos y spark streaming que aunque no es un sistema de streaming en el sentido estricto de la palabra, podemos realizar microbaches de pocos segundos (depende del ttl de los mensajes de kafka). El streaming parsea los mensajes y los almacena en el datawarehouse.


----------


Procesamiento de datos
--------------------

Para la generación de un modelo de ranking que permita a negocio ofrecer anuncios ordenados por la calidad de los mismos.
El score del anuncio se define mediante un sistema de radarchart cuyos ejes son:
Precio de mercado: (económico, normal, alto)
Antigüedad: (muy antiguo, antiguo, nuevo)
Kilometraje: (>100000, 100000>50000,50000>)



