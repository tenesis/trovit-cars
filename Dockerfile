FROM gettyimages/spark:2.1.1-hadoop-2.7

WORKDIR /kafka-feed
COPY . .

RUN echo "deb https://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list && \
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823 && \
    apt-get update && \
    apt-get install sbt && \
RUN sbt package

ENTRYPOINT /kafka-feed/spark-submit.sh