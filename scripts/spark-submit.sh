#!/bin/bash
spark_master=spark://$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' spark_master):7077
#spark_master=spark://master:7077
cd ..
spark-submit --class com.trovit.cars.datalakefeed.KafkaFeedsSpark \
--master  $spark_master\
--packages org.apache.spark:spark-streaming-kafka-0-8_2.11:2.1.1,org.mongodb.spark:mongo-spark-connector_2.11:2.2.1,org.json4s:json4s-native_2.11:3.5.3,com.typesafe:config:1.3.0 \
--executor-memory 1G \
--total-executor-cores 2 datalake-feed/target/scala-2.11/datalake-feed_2.11-0.2.jar $spark_master