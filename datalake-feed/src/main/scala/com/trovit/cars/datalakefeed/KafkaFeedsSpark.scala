package com.trovit.cars.datalakefeed

import org.apache.log4j.{Level, LogManager}

object KafkaFeedsSpark extends SparkApp {

  val log = LogManager.getRootLogger
  log.setLevel(Level.INFO)

  log.info("App initializing...")
  runJob(CarsFeedJob)


  private def runJob(app: App): Unit = {
    log.info("-----------------")
    log.info("Running job " + app.getClass.getSimpleName)
    app.main(Array())
    log.info("Job finished")
    log.info("-----------------")
  }

  override def getAppName = "Feed"
}
