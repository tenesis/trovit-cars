package com.trovit.cars.datalakefeed.repository

import java.time.{LocalDateTime, ZoneId}

import com.trovit.cars.datalakefeed.domain.repository.DataLakeRepository
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._


class DataLakeRepositoryImp() extends DataLakeRepository {


  override def saveCarsRdd(rDD: RDD[String]): Unit = {
    val sqlContext = SparkSession.builder.config(rDD.sparkContext.getConf).getOrCreate()

    val inputDateTime = LocalDateTime
      .now(ZoneId.of("Europe/Madrid"))

    // Default is previous day
    val year = inputDateTime.getYear
    val month = inputDateTime.getMonthValue
    val day = inputDateTime.getDayOfMonth

    val data = sqlContext.read
      .json(rDD)
      .toDF
      .withColumn("year", lit(year))
      .withColumn("month", lit(month))
      .withColumn("day", lit(day))

    data.write
      .option("compression", "gzip")
      .mode("append")
      //      .partitionBy("year", "month", "day")
      .parquet(s"/user/trovit/car_ads")
  }
}
