package com.trovit.cars.datalakefeed

import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}

trait SparkApp extends App {

  def getAppName: String

  private lazy val conf: SparkConf =
    new SparkConf().set("spark.serializer", "org.apache.spark.serializer.KryoSerializer").setAppName(getAppName).setMaster(masterUrl)
  lazy val sparkSession: SparkSession = SparkSession
    .builder()
    .config(conf)
    .getOrCreate()

  lazy val sparkContext: SparkContext = sparkSession.sparkContext
  lazy val streamingContext = new StreamingContext(conf, Seconds(30))

  private def masterUrl: String = {
    val defaultMasterUrl = "local[1]"
    if (args == null || args.isEmpty) {
      defaultMasterUrl
    } else {
      Option(args(0)).getOrElse(defaultMasterUrl)
    }
  }
}
