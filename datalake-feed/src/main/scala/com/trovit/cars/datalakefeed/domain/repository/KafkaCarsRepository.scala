package com.trovit.cars.datalakefeed.domain.repository

import org.apache.spark.streaming.dstream.DStream
import org.bson.Document

trait KafkaCarsRepository {
  def getCarsJsonStream(): DStream[String]
  def getDfCarsDocument(): DStream[Document]
}
