package com.trovit.cars.datalakefeed.domain.entities

case class Cars(
                 country: String,
                 _id: String,
                 urlAnonymized: String,
                 make: String,
                 model: String,
                 year: Double,
                 mileage: Double,
                 price: Double,
                 doors: Double,
                 region: String,
                 city: String,
                 date: String,
                 titleChunk: String,
                 contentChunk: String
               )
