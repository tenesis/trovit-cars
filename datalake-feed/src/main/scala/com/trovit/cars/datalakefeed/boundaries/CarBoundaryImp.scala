package com.trovit.cars.datalakefeed.boundaries

import com.trovit.cars.datalakefeed.domain.boundaries.CarBoundary
import com.trovit.cars.datalakefeed.domain.entities.Cars
import org.bson.Document

class CarBoundaryImp extends CarBoundary {

  override def getCarFromJson(json: String): Cars = {
//    parse(json).extract[Cars]
    null
  }

  override def getCarDocumentFromJson(json: String): Document = {
    Document.parse(json)
  }
}
