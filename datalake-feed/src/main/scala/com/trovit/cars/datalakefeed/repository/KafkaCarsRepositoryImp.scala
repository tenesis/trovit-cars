package com.trovit.cars.datalakefeed.repository


import com.trovit.cars.datalakefeed.domain.boundaries.CarBoundary
import com.trovit.cars.datalakefeed.domain.repository.KafkaCarsRepository
import com.typesafe.config.ConfigFactory
import kafka.serializer.StringDecoder
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.kafka.KafkaUtils
import org.bson.Document

import scala.util.Try

class KafkaCarsRepositoryImp(carBoundary: CarBoundary, sparkContext: StreamingContext) extends KafkaCarsRepository {


  override def getCarsJsonStream(): DStream[String] = {

    val kafkaConf = Map("metadata.broker.list" -> ConfigFactory.load().getString("app.broker_list"))

    val items = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](
      sparkContext, kafkaConf, Set("cars_feed"))

    items.map[String] { case (x, y) => {
      val item_replaced = y.replaceAll("\"", "'")
      item_replaced.replace("uniqueId", "_id")
    }
    }
  }

  override def getDfCarsDocument(): DStream[Document] = {
    val kafkaConf = Map("metadata.broker.list" -> ConfigFactory.load().getString("app.broker_list"))

    val items = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](
      sparkContext, kafkaConf, Set("cars_feed"))

    items.map[String] { case (x, y) => {
      val item_replaced = y.replaceAll("\"", "'")
      item_replaced.replace("uniqueId", "_id")
    }
    }.map {
      json => Try(Document.parse(json))
    }.filter(_.isSuccess).map(_.get)
  }
}
