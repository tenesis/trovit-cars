package com.trovit.cars.datalakefeed.domain.boundaries

import com.trovit.cars.datalakefeed.domain.entities.Cars
import org.bson.Document

trait CarBoundary {
  def getCarFromJson(json: String): Cars

  def getCarDocumentFromJson(json: String): Document
}
