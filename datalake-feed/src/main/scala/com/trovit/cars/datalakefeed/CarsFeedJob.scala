package com.trovit.cars.datalakefeed

import com.trovit.cars.datalakefeed.boundaries.CarBoundaryImp
import com.trovit.cars.datalakefeed.domain.repository.{DataLakeRepository, KafkaCarsRepository}
import com.trovit.cars.datalakefeed.repository.{DataLakeRepositoryImp, KafkaCarsRepositoryImp}
import org.apache.log4j.LogManager

object CarsFeedJob extends SparkApp {

  //TODO Dependency injection.
  //------------------- Dependencies -------------------
  val log = LogManager.getRootLogger
  val carsBoundary = new CarBoundaryImp()
  val carsRepository: KafkaCarsRepository = new KafkaCarsRepositoryImp(carsBoundary, streamingContext)
  //  val dataWareHouseRepository: DataWareHouseRepository = new DataWareHouseRepositoryImp()

  val cars = carsRepository.getCarsJsonStream()
  log.info("Saving on datalake!")
  cars.foreachRDD(rdd => {
    val dataLakeRepository: DataLakeRepository = new DataLakeRepositoryImp()
    dataLakeRepository.saveCarsRdd(rdd)
  }
  )
  streamingContext.start()
  streamingContext.awaitTermination()
  log.info("Finish")

  override def getAppName = "CarsFeed"
}
