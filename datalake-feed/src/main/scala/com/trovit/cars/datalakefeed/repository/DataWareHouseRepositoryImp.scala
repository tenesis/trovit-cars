package com.trovit.cars.datalakefeed.repository

import com.mongodb.spark.config._
import com.trovit.cars.datalakefeed.domain.repository.DataWareHouseRepository
import com.typesafe.config.ConfigFactory
import org.apache.spark.rdd.RDD
import com.mongodb.spark._
import org.bson.Document

object DataWareHouseRepositoryImp extends DataWareHouseRepository {

  val mongoOut: String = ConfigFactory.load().getString("app.mongo_uri") + "." + ConfigFactory.load().getString("app.mongo_cars_collection")

  override def saveRdd(rDD: RDD[Document]): Unit = {
    rDD.saveToMongoDB(WriteConfig(Map("uri" -> mongoOut)))
  }
}
