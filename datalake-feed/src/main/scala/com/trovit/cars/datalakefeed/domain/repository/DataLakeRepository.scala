package com.trovit.cars.datalakefeed.domain.repository

import org.apache.spark.rdd.RDD

trait DataLakeRepository {
  def saveCarsRdd(rDD: RDD[String])
}
