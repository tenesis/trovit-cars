package com.trovit.cars.datalakefeed.domain.repository

import org.apache.spark.rdd.RDD
import org.bson.Document

trait DataWareHouseRepository {
  def saveRdd(rDD: RDD[Document]): Unit
}
