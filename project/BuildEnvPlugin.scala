import sbt.Keys._
import sbt._
import sbt.plugins.JvmPlugin

object BuildEnvPlugin extends AutoPlugin {

  override def trigger: AllRequirements.type = AllRequirements

  override def requires: JvmPlugin.type = JvmPlugin

  object autoImport {

    object BuildEnv extends Enumeration {
      val Live, Pre, Dev = Value
    }

    val buildEnv: SettingKey[BuildEnv.Value] = settingKey[BuildEnv.Value](" the current build environment")
  }

  import autoImport._

  override def projectSettings: Seq[Setting[_]] = Seq(
    buildEnv := {
      sys.props.get("env")
        .orElse(sys.env.get("BUILD_ENV"))
        .flatMap {
          case "live" => Some(BuildEnv.Live)
          case "pre" => Some(BuildEnv.Pre)
          case "dev" => Some(BuildEnv.Dev)
          case _ => None
        }
        .getOrElse(BuildEnv.Dev)
    },

    onLoadMessage := {
      // depend on the old message as well
      val defaultMessage = onLoadMessage.value
      val env = buildEnv.value
      s"""|$defaultMessage
          |Awemose! Running in environment: $env""".stripMargin
    }
  )


}
