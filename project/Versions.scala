object Versions {
  val spark = "2.1.1"
  val sparkTestingBase = "2.1.0_0.6.0"
  val pprint = "0.5.0"
  val scalaTest = "3.0.3"
  val scalaMock = "3.5.0"
  val scalaCheck = "1.13.4"
  val mongo = "2.1.0"
  val json4s = "3.5.3"
  val typesafe = "1.3.0"
}